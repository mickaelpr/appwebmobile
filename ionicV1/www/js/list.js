monApp.controller('listController', ['$scope', '$http', '$window', '$state', function($scope, $http, $window, $state){
    $scope.refreshList = function(){
        $http({
            method: 'GET',
            url: serverNode + '/list/getSetByUserId/'+ $scope.user._id // TODO : a implémenter
        }).then(function successCb(response){
            console.log(response.data);
            $scope.laliste = response.data;
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };
    
    $scope.initialize = function(){
        var list = $window.localStorage.getItem('List');
        var user = {
            _id : $window.localStorage.getItem('User._id'),
            pseudo : $window.localStorage.getItem('User.pseudo')
        }
        if(list){
            $window.localStorage.removeItem('List');
        }
        if(user._id && user.pseudo){
            $scope.user = user
            $scope.formData = {};
            
            $scope.refreshList();
        } else {
            $state.go('connexion');
        }
    };
    
    $scope.createList = function(){
        var newlist = {
            nom: $scope.formData.nom,
            description: $scope.formData.description,
            creator: $scope.user,
            contributors: []
        };

        $http({
            method: 'POST',
            url: serverNode + '/list/post',
            data: newlist
        }).then(function successCb(response){
            $scope.formData = {};
            $scope.refreshList();
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    $scope.deleteList = function(id){
        $http({
            method: 'DELETE',
            url: serverNode + '/task/deleteTasksByListId/' + id
        }).then(function successCb(response){
            console.log("Suppression des tâches avec succès!!");
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
        
        $http({
            method: 'DELETE',
            url: serverNode + '/list/deleteById/' + id
        }).then(function successCb(response){
            $scope.refreshList();
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    $scope.enterList = function(id){
        $window.localStorage.setItem('List', id);
        $state.go('todo');
    };

    $scope.disconnect = function(){
        $window.localStorage.removeItem('User._id');
        $window.localStorage.removeItem('User.pseudo');
        $state.go('connexion');
    };

    $scope.initialize();
}]);
