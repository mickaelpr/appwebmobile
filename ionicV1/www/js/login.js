monApp.controller('loginController', ['$scope', '$http', '$window', '$state', function($scope, $http, $window, $state){
    $scope.initialize = function(){
        $scope.user = {};
        $scope.errorMessage = false;
        $scope.successMessage = false;
        var user = {
            _id : $window.localStorage.getItem('User._id'),
            pseudo : $window.localStorage.getItem('User.pseudo')
        }
        if(user._id && user.pseudo){
            console.log(user);
            $state.go('todo');
        }
    };

    $scope.register = function(){
        $scope.errorMessage = false;
        $scope.successMessage = false;
        if(!$scope.user.pseudo || !$scope.user.password){
            $scope.errorMessage = "Veuillez insérer toutes les informations!";
        } else {
            $http({
                method: 'POST',
                url: serverNode + '/user/post',
                data: $scope.user
            }).then(function successCb(response){
                $scope.user = {};
                $scope.errorMessage = false;
                $scope.successMessage = "Félicitation, vous avez réussi à créer votre compte!";
            }, function errorCb(response){
                $scope.errorMessage = response.data.error;
                $scope.successMessage = false;
            });
        }
    };

    $scope.connect = function(){
        $scope.errorMessage = false;
        $http({
            method: 'POST',
            url: serverNode + '/user/connexion',
            data: $scope.user
        }).then(function successCb(response){
            $window.localStorage.setItem('User._id', response.data.user._id);
            $window.localStorage.setItem('User.pseudo', response.data.user.pseudo);
            $state.go('list');
        }, function errorCb(response){
            $scope.errorMessage = response.data.error;
        });

    };

    $scope.disconnect = function(){
        $window.localStorage.removeItem('User._id');
        $window.localStorage.removeItem('User.pseudo');
        $state.go('connexion');
    };

    $scope.initialize();
}]);
