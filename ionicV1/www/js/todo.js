monApp.controller('todoController', ['$scope', '$http', '$window', '$state', function($scope, $http, $window, $state){
    $scope.updateList = function(){
        $http({
            method: 'GET',
            url: serverNode + '/task/getSetById/' + $window.localStorage.getItem('List')
        }).then(function successCb(response){
            $scope.laliste = response.data;
            
            $scope.updatePercent();
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    $scope.initialize = function(){
        var user = {
            _id : $window.localStorage.getItem('User._id'),
            pseudo : $window.localStorage.getItem('User.pseudo')
        }
        var list = $window.localStorage.getItem('List');
        if(user._id && user.pseudo){
            if(list){
                $scope.user = user;
                $scope.list_id = list;
                $scope.formData = {};
                $scope.updateList();
            } else {
                $state.go('list');
            }
        } else {
            $state.go('connexion');
        }
    };  
    
    
    $scope.updatePercent = function(){
        var count = 0;
        if($scope.laliste.length == 0){
            $scope.percent = 100 + "%";
        } else {
            $scope.laliste.forEach(function(task){
                if(task.done) count++;
            });
            var percent = (count / $scope.laliste.length)*100;
            percent = Math.ceil(percent);
            $scope.percent = percent + "%";
        }
    };

    $scope.createTodo = function(){
        console.log($scope.user);
        $scope.formData.author = $scope.user.pseudo;
        $scope.formData.list_id = $scope.list_id;
        $http({
            method: 'POST',
            url: serverNode + '/task/post',
            data: $scope.formData
        }).then(function successCb(response){
            $scope.formData = {};
            $scope.updateList();
            $scope.updatePercent();
        }, function errorCb(response){
            console.log('Error: ' + response.data.error);
        });
    };

    $scope.checkDone = function(data){
        data.done = !data.done;
        data.list_id = $scope.list_id;
        console.log(data);
        $http({
            method: 'PUT',
            url: serverNode + '/task/update',
            data: data
        }).then(function successCb(response){
            $scope.updateList();
            console.log(response.data);
            $scope.updatePercent();            
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    }

    $scope.deleteTodo = function(id){
        $http({
            method: 'DELETE',
            url: serverNode + '/task/delete/' + id
        }).then(function successCb(response){
            $scope.updateList();
            $scope.updatePercent();
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    $scope.modifyTodo = function(data){
        data.author = $scope.user.pseudo;
        $http({
            method: 'PUT',
            url: serverNode + '/task/update',
            data: data
        }).then(function successCb(response){
            $scope.updateList();
            console.log(response.data);
            $scope.updatePercent();            
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    $scope.goBack = function(){
        $window.localStorage.removeItem('List');
        $state.go('list');
    };
    
    $scope.disconnect = function(){
        $window.localStorage.removeItem('User._id');
        $window.localStorage.removeItem('User.pseudo');
        $state.go('connexion');
    };

    $scope.initialize();
    
}]);
