var dataLayer = require("./datalayer");
ObjectID = require('mongodb').ObjectID;
var bcrypt = require('bcrypt');
var saltRounds = 10;

var appRoute = function(app){
    
    app.get('/', function(req, res) {
        res.sendFile('../index.html');
    });

    /*--- Tasks ---*/
    app.get('/api/task/getAllTaskSet', function(req, res) {
        dataLayer.getTaskSet(function(laliste) {
            res.send(laliste);
        });
    });

    app.get('/api/task/getSetById/:id', function(req, res) {
        var ident = {
            list_id : new ObjectID(req.params.id)
        };
        dataLayer.getTaskSetByID(ident, function(laliste) {
            res.send(laliste);
        });
    });

    app.post('/api/task/post', function(req, res){
        if(req.body.author){
            author = req.body.author;
            dataLayer.createTask({
                text : req.body.text,
                date : new Date(),
                author : author,
                list_id : new ObjectID(req.body.list_id),
                done : false
            }, function(err, liste){
                return res.send();                
            });
        }
    });

    app.put('/api/task/update', function(req, res){
        var id = new ObjectID(req.body._id);
        var ident = {
            _id : id
        };
        var task = {
            text: req.body.text,
            author: req.body.author,
            done: req.body.done
        };

        dataLayer.updateTask(ident, task, function(err, result){
            if(err)
                res.send(err);
            dataLayer.getTaskSet(function(laliste) {
                res.send(laliste);
            });
        });
    });

    app.delete('/api/task/delete/:id', function(req, res){
        var ident = {
            _id : new ObjectID(req.params.id)
        };
        dataLayer.deleteTask(ident, function(err, liste){
            if(err)
                res.send(err);
            dataLayer.getTaskSet(function(laliste) {
                res.send(laliste);
            });
        });
    });

    app.delete('/api/task/deleteTasksByListId/:id', function(req, res){
        var ident = {
            "list_id" : new ObjectID(req.params.id)
        };
        console.log(ident);
        dataLayer.deleteTasksByListId(ident, function(err, liste){
            if(err)
                res.send(err);
            res.send();
        });
    });

    /*--- Lists ---*/
    app.get('/api/list/getSet', function(req, res) {
        dataLayer.getListSet(function(laliste) {
            res.send(laliste);
        });
    });

    app.get('/api/list/getSetByUserId/:id', function(req, res) {
        console.log(req.params.id);
        var ident = req.params.id;
        dataLayer.getListSetFromUserId(ident, function(laliste) {
            res.send(laliste);
        });
    });

    app.post('/api/list/post', function(req, res){
        req.body.creator._id = req.body.creator._id;
        console.log(req.body);
        dataLayer.createList({
            _id: new ObjectID(),
            nom: req.body.nom,
            description: req.body.description,
            creator: req.body.creator,
            contributors: req.body.contributors,
            date : new Date()
        }, function(err, liste){
            if(err)
                res.send(err);
            dataLayer.getListSet(function(laliste) {
                res.send(laliste);
            });
        });
    });

    app.put('/api/list/update', function(req, res){
        // Partie utilisateur à ajouter
        var user = req.body.user;

        // Partie liste à modifier
        var list = req.body.list;
        list._id = new ObjectID(list._id);
        list.contributors.push(user);
        
        var ident = {
            _id : user._id
        };
        var new_list = {
            contributors: list.contributors
        };

        dataLayer.updateList(ident, new_list, function(err, result){
            if(err)
                res.send(err);
            res.status(200).send();
        });
    });

    app.delete('/api/list/deleteById/:id', function(req, res){
        var ident = {
            _id : new ObjectID(req.params.id)
        };
        dataLayer.deleteList(ident, function(err, liste){
            if(err)
                res.send(err);
            dataLayer.getListSet(function(laliste) {
                res.send(laliste);
            });
        });
    });

    /*--- Users ---*/

    app.get('/api/user/getSet', function(req, res) {
        dataLayer.getUserSet(function(laliste) {
            res.status(200).json({'data' : laliste});
        });
    });

    app.post('/api/user/getByPseudo', function(req, res){
        console.log("debut api-route /getUser");
        dataLayer.getUser(req.body.pseudo, function(user){
            console.log('res: ' + user);
            if(user){
                var newuser = {
                    _id : user._id,
                    pseudo : user.pseudo
                };
                return res.status(200).json({'user' : newuser});
            } else {
                return res.status(401).json({'error': "Aucun utilisateur trouvé"});
            }
        });
    });

    app.post('/api/user/post', function(req, res){
        dataLayer.getUser(req.body.pseudo, function(user){
            if(!user){
                bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                    dataLayer.createUser({
                        pseudo : req.body.pseudo,
                        password : hash
                    }, function(err, user){
                        res.status(200);
                        res.send();
                    });
                });
            } else {
                res.status(401).json({'error': "Utilisateur déjà existant"});
            }
        });
        
    });

    app.post('/api/user/connexion', function(req, res){
        var user = {
            pseudo : req.body.pseudo,
            password : req.body.password
        };
        dataLayer.getUser(user.pseudo, function(getuser){
            console.log(getuser);

            if(getuser){
                bcrypt.compare(user.password, getuser.password, function(err, resultat) {
                    if(err) throw err;
                    if(resultat == true) {
                        newuser = {
                            _id : getuser._id,
                            pseudo : getuser.pseudo
                        };
                        res.status(200).json({'user' : newuser});
                    } else {
                        res.status(400).json({'error': 'Login ou mot de passe incorrect'});
                    }
                });
            } else {
                res.status(400).json({'error': 'Login ou mot de passe incorrect'});
            }
        });
    });

    
}

module.exports = appRoute;