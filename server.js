var express = require('express');
var app = express();
// Morgan : pour la console
var morgan = require('morgan');
var bodyParser = require('body-parser');
var route = require('./api-route');

var dataLayer = require('./datalayer');

app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-with, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

route(app);


dataLayer.init(function(){
    console.log('init');
    const PORT = process.env.PORT || 8080;
    app.listen(PORT);
    console.log("Listening on port 8080...");
});
