var todoApp = angular.module('todoApp', []);
todoApp.controller('TodoCtrl', ['$scope', function($scope){
    // Initialisation du tableau de tâches
    $scope.taskSet = [
        {
            taskName: 'Faire la vaisselle',
            done: false
        },
        {
            taskName: 'Faire les courses',
            done: true
        },
        {
            taskName: 'Nourir les sims',
            done: false
        }
    ];

    // Méthode pour ajouter une tâche
    $scope.addTask = function(){
        if($scope.taskName !== ''){
            // Ajout de la tâche au tableau
            $scope.taskSet.push({
                taskName: $scope.taskName,
                done: false
            });
            
            // On enlève la valeur de taskName
            $scope.taskName = ''; 
        }
    }

    $scope.checkDone = function(task){
        task.done = !task.done;
    }

    $scope.remove = function(task) { 
        var index = $scope.taskSet.indexOf(task);
        $scope.taskSet.splice(index, 1);     
    }
    

}]);

