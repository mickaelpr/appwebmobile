var app = angular.module('List', ['ngCookies']);

app.controller('listController', ['$scope', '$http', '$window', '$cookies', function($scope, $http, $window, $cookies){
    $scope.refreshList = function(){
        $http({
            method: 'GET',
            url: '/api/list/getSetByUserId/'+ $scope.user._id // TODO : a implémenter
        }).then(function successCb(response){
            console.log(response.data);
            $scope.laliste = response.data;
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };
    
    $scope.initialize = function(){
        if($cookies.getObject("List")){
            $cookies.remove("List");
        }
        if($cookies.getObject("User")){
            $scope.user = $cookies.getObject("User");
            $scope.formData = {};
            
            $scope.refreshList();
        } else {
            $window.location.replace('/');
        }
    };
    
    $scope.createList = function(){
        var newlist = {
            nom: $scope.formData.nom,
            description: $scope.formData.description,
            creator: $scope.user,
            contributors: []
        };

        $http({
            method: 'POST',
            url: '/api/list/post',
            data: newlist
        }).then(function successCb(response){
            $scope.formData = {};
            $scope.refreshList();
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    $scope.deleteList = function(id){
        $http({
            method: 'DELETE',
            url: '/api/task/deleteTasksByListId/' + id
        }).then(function successCb(response){
            console.log("Suppression des tâches avec succès!!");
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
        
        $http({
            method: 'DELETE',
            url: '/api/list/deleteById/' + id
        }).then(function successCb(response){
            $scope.refreshList();
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    $scope.enterList = function(id){
        $cookies.putObject("List", id);
        $window.location.replace('/todo.html');
    };

    $scope.disconnect = function(){
        $cookies.remove("User");
        $window.location.replace('/');
    };

    $scope.initialize();
}]);
