var app = angular.module('TodoList', ['ngCookies']);

app.controller('todoController', ['$scope', '$http', '$window', '$cookies', function($scope, $http, $window, $cookies){
    $scope.updateList = function(){
        // TODO : implémenter avec $scope.list_id
        $http({
            method: 'GET',
            url: '/api/task/getSetById/' + $cookies.getObject("List")
        }).then(function successCb(response){
            $scope.laliste = response.data;
            
            $scope.updatePercent();
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    $scope.initialize = function(){
        if($cookies.getObject("User")){
            if($cookies.getObject("List")){
                $scope.user = $cookies.getObject("User");
                $scope.list_id = $cookies.getObject("List");
                $scope.formData = {};
                $scope.updateList();
            } else {
                $window.location.replace('/list.html');
            }
        } else {
            $window.location.replace('/');
        }
    };  
    
    
    $scope.updatePercent = function(){
        var count = 0;
        if($scope.laliste.length == 0){
            $scope.percent = 100 + "%";
        } else {
            $scope.laliste.forEach(function(task){
                if(task.done) count++;
            });
            var percent = (count / $scope.laliste.length)*100;
            percent = Math.ceil(percent);
            $scope.percent = percent + "%";
        }
    };

    $scope.createTodo = function(){
        console.log($scope.user);
        $scope.formData.author = $scope.user.pseudo;
        $scope.formData.list_id = $scope.list_id;
        $http({
            method: 'POST',
            url: '/api/task/post',
            data: $scope.formData
        }).then(function successCb(response){
            $scope.formData = {};
            $scope.updateList();
            $scope.updatePercent();
        }, function errorCb(response){
            console.log('Error: ' + response.data.error);
        });
    };

    $scope.checkDone = function(data){
        data.done = !data.done;
        data.list_id = $scope.list_id;
        console.log(data);
        $http({
            method: 'PUT',
            url: '/api/task/update',
            data: data
        }).then(function successCb(response){
            $scope.updateList();
            console.log(response.data);
            $scope.updatePercent();            
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    }

    $scope.deleteTodo = function(id){
        $http({
            method: 'DELETE',
            url: '/api/task/delete/' + id
        }).then(function successCb(response){
            $scope.updateList();
            $scope.updatePercent();
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    $scope.modifyTodo = function(data){
        data.author = $scope.user.pseudo;
        $http({
            method: 'PUT',
            url: '/api/task/update',
            data: data
        }).then(function successCb(response){
            $scope.updateList();
            console.log(response.data);
            $scope.updatePercent();            
        }, function errorCb(response){
            console.log('Error: ' + response.data);
        });
    };

    
    $scope.shareList = function(liste){
        // TODO gérer avec les modals
    };

    $scope.addContributors = function(laliste, pseudoContributor){
        // get user
        $http({
            method: 'POST',
            url: '/api/user/getByPseudo',
            data: pseudoContributor
        }).then(function successCb(response){
            var user = response.data.user;
            // add user to contributors
            //laliste.contributors.push(user);
            // requete put list

        }, function errorCb(response){
            console.log('Error: ' + response.data);
            // TODO : mettre message d'erreur dans une alert
        });
        
    };

    $scope.goBack = function(){
        $cookies.remove("List");
        $window.location.replace('/list.html');
    };
    
    $scope.disconnect = function(){
        $cookies.remove("User");
        $window.location.replace('/');
    };

    $scope.initialize();
    
}]);
