var app = angular.module('LoginApp',['ngCookies']);

app.controller('loginController', ['$scope', '$http', '$window', '$cookies', function($scope, $http, $window, $cookies){
    $scope.initialize = function(){
        $scope.user = {};
        $scope.errorMessage = false;
        $scope.successMessage = false;
        if($cookies.getObject("User")){
            u = $cookies.getObject("User");
            console.log(u);
            $window.location.replace('/todo.html');
        }
    };

    $scope.register = function(){
        $scope.errorMessage = false;
        $scope.successMessage = false;
        if(!$scope.user.pseudo || !$scope.user.password){
            $scope.errorMessage = "Veuillez insérer toutes les informations!";
        } else {
            $http({
                method: 'POST',
                url: '/api/user/post',
                data: $scope.user
            }).then(function successCb(response){
                $scope.user = {};
                $scope.errorMessage = false;
                $scope.successMessage = "Félicitation, vous avez réussi à créer votre compte!";
            }, function errorCb(response){
                $scope.errorMessage = response.data.error;
                $scope.successMessage = false;
            });
        }
    };

    $scope.connect = function(){
        $scope.errorMessage = false;
        $http({
            method: 'POST',
            url: '/api/user/connexion',
            data: $scope.user
        }).then(function successCb(response){
            $cookies.putObject("User", response.data.user);
            $window.location.replace('/list.html');
        }, function errorCb(response){
            $scope.errorMessage = response.data.error;
        });

    };

    $scope.disconnect = function(){
        $cookies.remove("User");
        $window.location.replace('/');
    };

    $scope.initialize();
}]);
