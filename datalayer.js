var MongoClient = require('mongodb').MongoClient;
ObjectID = require('mongodb').ObjectID;
var uri = "mongodb+srv://user1:user1@cmappmobile-6kmz3.gcp.mongodb.net/test?retryWrites=true";
var client = new MongoClient(uri, {userNewUrlParser: true});
var db;

var databaseName = "TodoList";
var taskTable = "Task";
var userTable = "User";
var listTable = "List";

var dataLayer = {
    init : function(cb){
        client.connect(function(err){
            if(err) throw err;
            db = client.db(databaseName);
            cb();
        });
    },

    getListSet : function(cb){
        db.collection(listTable).find({}).toArray(function(err, lists){
            if(err) throw err;
            cb(lists);
        });
    },

    getTaskSet : function(cb){
        db.collection(taskTable).find({}).toArray(function(err, tasks){
            if(err) throw err;
            console.log(tasks);
            cb(tasks);
        });
    },

    getTaskSetByID : function(id, cb){
        db.collection(taskTable).find(id, {}).toArray(function(err, tasks){
            if(err) throw err;
            console.log(tasks);
            cb(tasks);
        });
    },

    getUserSet : function(cb){
        db.collection(userTable).find({}).toArray(function(err, users){
            if(err) throw err;
            cb(users);
        });
    },

    getListSetFromUserId : function(userId, cb){
        var query = {
            /*$or: [ 
                {
                    'creator._id': userId
                },
                {
                    'contributor._id': userId 
                }
            ]*/
            
            'creator._id': userId
            
        };
        console.log("datalayer");
        console.log(query);
        db.collection(listTable).find(query, {}).toArray(function(err, lists){
            if(err) throw err;
            cb(lists);
        });
    },

    createList : function(list, cb){
        db.collection(listTable).insertOne(list, function(err, result){
            if(err) throw err;
            cb();
        });
    },

    createTask : function(task, cb){
        db.collection(taskTable).insertOne(task, function(err, result){
            cb();
        });
    },

    updateTask : function(ident, task, cb){
        db.collection(taskTable).updateOne(ident, {$set: task}, function(err, result){
            cb();
        });
    },

    updateList : function(ident, list, cb){
        console.log("ident");
        console.log(ident);
        console.log("list");
        console.log(list);
        db.collection(listTable).updateOne(ident, {$set: list}, function(err, result){
            cb();
        });
    },

    deleteTask : function(id, cb){        
        db.collection(taskTable).deleteOne(id, function(err, res){
            if(err) throw err;
            cb();
        });
    },

    deleteTasksByListId : function(id, cb){
        console.log("delete datalayer");
        res = db.collection(taskTable).deleteMany(id, function(err, res){
            if(err) throw err;
            cb();
        });
    },

    deleteList : function(id, cb){
        db.collection(listTable).deleteOne(id, function(err, res){
            if(err) throw err;
            cb();
        });
    },

    createUser : function(user, cb){
        console.log("CreateUser datalayer");
        db.collection(userTable).insertOne(user, function(err, resultat){
            if(err) throw err;
            cb();
        });
    },

    getUser : function(username, cb){
        db.collection(userTable).findOne({pseudo: username}, function(err, user){
            if(err) throw err;
            cb(user);
        });
    }
};

module.exports = dataLayer;