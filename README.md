# Application Web : Liste de choses à faire

Cette application est un projet créé pour le cours d'Application Web et Mobile,
à **Polytech Marseille** en 4ème année par **Mickaël PRADES**.

## Présentation de l'application

L'objectif est de développer une application web permettant de gérer des choses
à faire et possède d'autres fonctionnalités. 

Pour commencer, il est possible de créer un compte et de se connecter à 
l'application. Un compte est représenté par un identifiant et un mot de passe.

Une fois connecté, il est possible de créer une liste de liste de choses à faire,
on va pouvoir supprimer ou accéder à une liste en particulier.

Lorsque l'on accède à une liste, on trouve les tâches qui sont liés à cette dernière.
Les tâches créés sur cette liste n'apparaissent que sur cette liste. 
De plus, il est possible de modifier une tâche.

Pour finir, il est possible de revenir à la page de liste de listes en cliquant sur
son pseudonyme, ou encore de se déconnecter, en appuyant sur le bouton déconnecter.

## Caractéristiques de l'application

L'application utilise **NodeJS** pour le côté serveur, avec **ExpressJS**, et 
**AngularJS** pour le côté client.

Concernant la base de données, celle-ci est basée sur **MongoDB** et les requêtes
de l'application sont basées sur l'outil MongoDB.

Du côté de l'interface, l'application possède un côté web avec du responsive (
**Bootstrap**) et un côté mobile avec **Ionic**.

L'hébergement du site est fait gratuitement par l'hébergeur **Heroku**.

## Comment utiliser l'application

### Application web

Pour essayer et accéder à l'application, on peut visiter la dernière version sur
le projet Heroku :

>  https://todolist-poly4a.herokuapp.com/

Ou alors, on peut *cloner le repository* et lancer le serveur NodeJS :

`node server.js` ou `nodemon server.js`

**Attention**, il n'est pas possible d'utiliser l'application sans avoir de serveur.

### Application mobile

Pour Ionic, il est possible de lancer le serveur grâce à la commande :

`ionic serve`

Ou d'accéder au serveur hébergé :

>  https://ionic-todolist-poly4a.herokuapp.com/

**Attention**, il n'est pas possible d'utiliser l'application mobile **sans**
avoir de serveur Nodejs (voir partie précédente).

## Etapes :

### Création du projet :

*  [x] Faire un Gitlab
*  [x] Installer npm et tout ce qui va avec
*  [x] Initialiser le projet npm
*  [x] Créer ma première page *index.html*

### Liste de tâches :

*  [x] Créer une liste de tâches
*  [x] Rajouter la date et le nom/id de l'utilisateur à la tâche
*  [x] Créer la base de données MongoDB
*  [x] Créer et gérer les requêtes de base de données (avec *datalayer* et *api-route*)
*  [x] Ajouter une tâche dans la base de données
*  [x] Gérer la modification de données

### Authentification :

*  [x] Créer la partie HTML/CSS
*  [x] Gérer la création de compte
*  [x] Gérer la connexion
*  [x] Gérer les erreurs et *inputs* vides
*  [x] Créer et gérer un cookie d'authentification

### Liste de listes :

*  [x] Créer la partie HTML/CSS
*  [x] Gérer la déconnexion
*  [x] Gérer la création d'une liste
*  [x] Gérer la suppression d'une liste
*  [x] Gérer la suppression des données de la liste lorsqu'une liste est supprimée
*  [x] Créer et gérer un cookie de liste (permet de garder l'id de la liste choisie)

### Ionic :

*  [x] Initialiser le projet Ionic
*  [x] Créer la première page
*  [x] Gérer les requêtes HTTP
*  [x] Créer les pages et gérer les scripts JS avec les contrôleurs
*  [x] Gérer le stockage local (alternative aux cookies)
*  [ ] Améliorer le design des pages
*  [ ] Faire en sorte que les requêtes sur Heroku fonctionnent

La partie Ionic est testable en local (en utilisant `ionic serve`) mais l'hébergement
possède des problèmes de compatibilité pour les requêtes HTTP.





